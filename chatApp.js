const EventEmitter = require('events');

class ChatApp extends EventEmitter {
  /**
   * @param {String} title
   */
  constructor(title) {
    super();

    this.title = title;

    // Посылать каждую секунду сообщение
    setInterval(() => {
      this.emit('message', `${this.title}: ping-pong`);
  }, 1000);
  }
  // 2.1. В классе чата ChatApp добавить метод close
  close(){
    this.emit('close', 'Чат вконтакте закрылся :(');
  }
}

module.exports = {
  ChatApp,
  EventEmitter
}