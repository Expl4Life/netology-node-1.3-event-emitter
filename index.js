const EventEmitter = require('./chatApp').EventEmitter;
const ChatApp = require('./chatApp').ChatApp;

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');

let chatOnMessage = require('./chatOnMessage');
let readyToMessage = require('./readyToMessage');

webinarChat
  .on('message', chatOnMessage)
  // 1.1. Добавить webinarChat обработчик события message
  .on('message', readyToMessage);
facebookChat.on('message', chatOnMessage);
vkChat
  // 1.2. Для Вконтакте (vkChat) установить максимальное количество обработчиков событий, равное 2.
  .setMaxListeners(2)
  .on('message', chatOnMessage)
  // 1.3. Добавить обработчик 'Готовлюсь к ответу' из пункта 1.1 к чату Вконтакте.
  .on('message', readyToMessage)
  // 2.2 Для чата вконтакте (vkChat) добавить обработчик close
  .on('close', console.log);


// 2.3. Вызывать у чата вконтакте метод close().
vkChat.close();

// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
vkChat.removeListener('message', chatOnMessage);
}, 10000 );

// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
facebookChat.removeListener('message', chatOnMessage);
}, 15000 );

// Отписать webinar от ChatOnMessage
setTimeout( ()=> {
  console.log('Закрываю вебинар....');
webinarChat.removeListener('message', chatOnMessage);
}, 30000 );





